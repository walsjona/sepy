import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

# Project specific
from instruction_file import *
from math import *
import numpy as np


def Cube(pos):

    verticies = [
        [0.5, -0.5, -0.5],
        [0.5, 0.5, -0.5],
        [-0.5, 0.5, -0.5],
        [-0.5, -0.5, -0.5],
        [0.5, -0.5, 0.5],
        [0.5, 0.5, 0.5],
        [-0.5, -0.5, 0.5],
        [-0.5, 0.5, 0.5]
        ]

    edges = [
        [0,1],
        [0,3],
        [0,4],
        [2,1],
        [2,3],
        [2,7],
        [6,3],
        [6,4],
        [6,7],
        [5,1],
        [5,4],
        [5,7]
        ]

    surfaces = [
        [0,1,2,3],
        [3,2,7,6],
        [6,7,5,4],
        [4,5,1,0],
        [1,5,7,2],
        [4,0,3,6]
        ]

    colors = [
        [1,0,0],
        [0,1,0],
        [0,0,1],
        [0,0,0],
        [1,1,1],
        [0,1,1],
        [1,0,0],
        [0,1,0],
        [0,0,1],
        [0,0,0],
        [1,1,1],
        [0,1,1]
        ]
    verticies_pos=[]
    for vert in verticies:
        new_vert=vert
        for i in range(0,3):
            new_vert[i]=vert[i]+pos[i]

        verticies_pos.extend([new_vert])

    glBegin(GL_QUADS)
    for surface in surfaces:
        x = 0
        for vertex in surface:
            x += 1
            glColor3fv([0.4,0.4,0.4])
            glVertex3fv(verticies_pos[vertex])
    glEnd()
    glLineWidth(5)
    glBegin(GL_LINES)
    for edge in edges:
       for vertex in edge:
           glColor3fv([0, 0, 0])
           glVertex3fv(verticies_pos[vertex])
    glEnd()

    glPointSize(10.0)
    glBegin(GL_POINTS)
    for vert in verticies_pos:
         glColor3fv([0, 0, 0])
         glVertex3f(vert[0],vert[1],vert[2])
    glEnd()


def plot_vert(blocklist):
    glPointSize(10.0)
    glBegin(GL_POINTS)
    #vert_N = len(blocklist)

    for vert in blocklist:
        glColor3fv([0, 0, 0])
        glVertex3f(vert[0], vert[1], vert[2])

    glEnd()

def rotation_matrix(x_ang,y_ang,z_ang):

    # Import required
    import numpy as np

    # Convert to radians
    x_ang_rad = x_ang * pi /180
    y_ang_rad = y_ang * pi / 180
    z_ang_rad = z_ang * pi / 180

    # Around individual axis
    Rx = np.matrix([[1,0,0],[0,cos(x_ang_rad),-sin(x_ang_rad)],[0,sin(x_ang_rad),cos(x_ang_rad)]])
    Ry = np.matrix([[cos(y_ang_rad), 0, sin(y_ang_rad)], [0, 1, 0], [-sin(y_ang_rad), 0, cos(y_ang_rad)]])
    Rz = np.matrix([[cos(z_ang_rad), -sin(z_ang_rad), 0], [sin(z_ang_rad), cos(x_ang_rad), 0], [0, 0, 1]])

    # Combine
    RM = Rz*Ry*Rx

    return RM



def main():

    ## Testing section
    #read_instructions()
    blocklist=[]

    blocklist=read_instructions(blocklist, 'test.bdy')

    print(blocklist)
    rotation_matrix(10, 0, 0)

    ## pygame and openGL stuff
    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)

    # Set up Camera
    Camera_orgin = [0, 0, 0]
    Camera_angle = [0, 0]
    Camera_pitch = np.matrix([[1],[0], [0]])

    glTranslatef(0, 0, -40)

    # Main Display loop
    while True:
        # Get events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    if (pygame.key.get_mods() & KMOD_LSHIFT):
                        glTranslate(0.5, 0, 0)
                    else:
                        Camera_angle[0] = Camera_angle[0] + 10
                        Camera_pitch = rotation_matrix(0, -10, 0) * Camera_pitch
                        glRotate(10, 0, 1, 0)

                if event.key == pygame.K_RIGHT:
                    if (pygame.key.get_mods() & KMOD_LSHIFT):
                        glTranslate(-0.5, 0, 0)
                    else:
                        Camera_angle[0] = Camera_angle[0] - 10
                        Camera_pitch = rotation_matrix(0, 10, 0) * Camera_pitch
                        glRotate(-10, 0, 1, 0)

                if event.key == pygame.K_UP:
                    if (pygame.key.get_mods() & KMOD_LSHIFT):
                        glTranslate(0, -0.5, 0)
                    else:
                        glRotate(10, Camera_pitch[0], Camera_pitch[1], Camera_pitch[2])

                if event.key == pygame.K_DOWN:
                    if (pygame.key.get_mods() & KMOD_LSHIFT):
                        glTranslate(0, 0.5, 0)
                    else:
                        glRotate(-10, Camera_pitch[0], Camera_pitch[1], Camera_pitch[2])


            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 4:
                    glScale(1/0.9,1/0.9,1/0.9)
                    #glTranslate(0, 0, 1.0)
                if event.button == 5:
                    glScale(0.9,0.9,0.9)
                    #glTranslate(0, 0, -1.0)

                    # glRotatef(1, 3, 1, 1)

                    ##        x=glGetDoublev(GL_MODELVIEW_MATRIX)
                    ##        print(x)

            # OpenGL Display properties
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LESS);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glClearColor(.9,.9,.9,0)
            # glTranslate(0,0,0.1)

            # Object to Display
            plot_vert(blocklist)
            for pos in blocklist:
                Cube(pos)

            pygame.display.flip()
            pygame.time.wait(10)


main()

