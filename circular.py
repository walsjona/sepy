import instruction_file
import block
from math import *
import numpy as np


def pixel_circle(radius):

    int_circ=np.array([[radius[0], 0]])
    circ_i = 0;

    query_while = int_circ[circ_i] + np.array([-1, 1])

    while query_while[0]-query_while[1] >= 0:
        qry_point_1 = int_circ[circ_i] + np.array([0, 1])
        qry_point_2 = int_circ[circ_i] + np.array([-1, 1])

        qry_circ_1 = abs(sqrt(pow(qry_point_1[0],2) + pow(qry_point_1[1],2))-radius[0])
        qry_circ_2 = abs(sqrt(pow(qry_point_2[0],2) + pow(qry_point_2[1],2))-radius[0])

        if qry_circ_1 <= qry_circ_2:
            int_circ = np.append(int_circ, [qry_point_1],axis=0)
        else:
            int_circ = np.append(int_circ, [qry_point_2],axis=0)

        #print([qry_circ_1,qry_circ_2])
        print(int_circ)

        circ_i = circ_i +1

        query_while = int_circ[circ_i] + np.array([-1, 1])

    # Complete the Ark
    ark_circ = int_circ
    for comp_ark in range(len(int_circ)):
        ark_circ = np.append(ark_circ,[[int_circ[comp_ark][1],int_circ[comp_ark][0]]], axis=0)

    # complete the Semi
    half_circ=ark_circ
    for comp_half in range(len(ark_circ)):
        half_circ = np.append(half_circ, [[-ark_circ[comp_half][0], ark_circ[comp_half][1]]], axis=0)

    # Complete the Circle
    full_circ=half_circ
    for  comp_full in range(len(half_circ)):
        full_circ = np.append(full_circ, [[half_circ[comp_full][0], -half_circ[comp_full][1]]], axis=0)

    # Clean up list
    full_circ_sort=full_circ[full_circ[:,0].argsort()]
    print(full_circ[full_circ[:,0].argsort()])
    print(np.sort(full_circ,order = [0, 1]))
    #double_circ=[full_circ[0]
    for comp_double in len(full_circ)-1:
        print(full_circ[comp_double]==full_circ[comp_double+1])

    #Convert to correct orienation
    circ_out=[]
    for circ_line in full_circ:
        circ_out.append([0,circ_line[0],circ_line[1]])



    return circ_out

def add_cylinder(blocklist,instruct):

    # Set Variables
    centre = [0, 0, 0]
    radius = [10]
    extrude = [1]
    euler = [0, 0, 0]

    # Get arguments
    arguments = instruct.split(';')

    for arg in arguments:
        arg_split = arg.split('=')
        # print(arg)

        # Radius
        if arg_split[0] == 'radius':
            radius=instruction_file.read_no_bra(arg_split[1])
        # Centre
        if arg_split[0] == 'centre':
            centre=instruction_file.read_curly(arg_split[1])

    circ_out = pixel_circle(radius)

    blocklist = block.mergeblocklist(blocklist, circ_out)

    return blocklist