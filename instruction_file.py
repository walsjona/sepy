# block functions
import block
import circular

def read_curly(arg_in):
    arg_len = (len(arg_in) - 1)
    arg_string = arg_in[1:arg_len].split(',')

    arg_out=[]

    for bre in arg_string:
        print(bre)
        int(bre)
        arg_out.append(int(bre))



    return arg_out

def read_no_bra(arg_in):
    arg_string = arg_in.split(',')

    arg_out=[]

    for bre in arg_string:
        print(bre)
        int(bre)
        arg_out.append(int(bre))



    return arg_out

def read_instructions(blocklist,filename):
    with open(filename) as FILE:
        read_data = FILE.readlines()
    FILE.closed

    for line in read_data:
        if line[0] != '%':

            # Separate instructions into command and arguments
            instruct = line.split(':')

            # Identify Command
            if instruct[0] == 'add_block':
                print(instruct[1])
                blocklist = block.add(blocklist,instruct[1])

            elif instruct[0] == 'add_cylinder':
                print(instruct[1])
                blocklist = circular.add_cylinder(blocklist, instruct[1])


    return blocklist