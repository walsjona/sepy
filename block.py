import instruction_file

def create_mesh(block_range):
    # This method creates a 3D array of the new blocks to add
    blocklist_new=[]

    for xcor in range(block_range[0], block_range[3]+1):
        for ycor in range(block_range[1], block_range[4]+1):
            for zcor in range(block_range[2], block_range[5]+1):
                blocklist_new.append([xcor, ycor, zcor])

    return blocklist_new
def mergeblocklist(blocklist,blocklist_new):

    for newblock in blocklist_new:

        to_include = 1
        for compblock in blocklist:
            if newblock == compblock:
                to_include=0

        if to_include==1:
            blocklist.extend([newblock])

    return blocklist

def add(blocklist,instruct):

    # Set Variables
    block_range = [0, 0, 0, 0, 0, 0]

    # Get arguments
    arguments = instruct.split(';')

    for arg in arguments:
        arg_split=arg.split('=')
        #print(arg)

        if arg_split[0] == 'range':

            block_range=instruction_file.read_curly(arg_split[1])


    # create blocklist
    blocklist_new=create_mesh(block_range)

    #print(blocklist_new[1])

    blocklist = mergeblocklist(blocklist, blocklist_new)
    #blocklist.extend(blocklist_new)

    #print(sorted(blocklist,reverse=True))

    return blocklist

def subtract(blocklist,instruct):

    # Set Variables
    block_range = [-4, -2, 0, 4, 2, 0]

    # Get arguments
    arguments = instruct.split(';')

    for arg in arguments:
        arg_split=arg.split('=')
        #print(arg)

        if arg_split[0] == 'range':
            #print(arg_split[1])
            arg_len=(len(arg_split[1])-1)
            block_range_string = arg_split[1][1:arg_len].split(',')

            bri=0
            for bre in block_range_string:
                block_range[bri] = int(bre)
                bri = bri + 1